#include <string>

namespace Bongo
{
    template<typename T>
    bool IsEqual(T value, T expected, std::string name)
    {
        bool result = value == expected;
        auto resultString = result ? "Success! :-)" : "Failed :-(.";
        std::cout << "Expecting " << name << " to be '" << expected << "' and the actual value is '" << value << "'.  |  " << resultString << std::endl;

        return result;
    }
}
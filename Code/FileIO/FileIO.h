#ifndef BONGO_FILEIO_H
#define BONGO_FILEIO_H

#include <string>
#include <fstream>

namespace Bongo
{
	class FileIO
	{
	public:
		bool Exists(std::string fileName)
		{
			std::ifstream file(fileName, std::ios::in);
			
			if (file)
			{
				path = fileName;
				return true;
			}

			return false;
		}

		std::string Read()
		{
			std::ifstream file(path, std::ios::in);
			std::string contents;

			if (file)
			{
				file.seekg(0, std::ios::end);
				contents.resize(file.tellg());

				file.seekg(0, std::ios::beg);
				file.read(&contents[0], contents.size());
				file.close();
				return contents;
			}

			return contents;
		}

		bool Write(std::string text)
		{
			std::ofstream file(path, std::ios::out);

			if (file)
			{
				file.write(text.c_str(), text.size());
				file.close();
				return true;
			}

			return false;
		}

	private:
		
		std::string path;

	};
}

#endif